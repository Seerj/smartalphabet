var tID;
function start(){
  tID = setInterval(letters, 100);
}

function letters(){
  
  var letter = document.querySelector("#litere").innerText;
  var charAscii = Number(letter.charCodeAt());
  if(charAscii >= 90){
    charAscii = 65;
  } else {
    charAscii++;
  }
  document.querySelector("#litere").innerText = String.fromCharCode(charAscii);
  document.querySelector("#litere").style.color = "rgb(" + Math.floor(Math.random()*256) + "," + Math.floor(Math.random()*256) + "," + Math.floor(Math.random()*256) +")";
}

function stop(){
  clearInterval(tID);
}
